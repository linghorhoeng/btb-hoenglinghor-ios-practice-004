//
//  ViewController.swift
//  Practice-Facebook-Login
//
//  Created by Hoeng Linghor on 11/26/20.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth
import AuthenticationServices

class ViewController: UIViewController {

    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        let loginButton = FBLoginButton()
//               loginButton.center = view.center
//               view.addSubview(loginButton)
    }
    @IBAction func clickLogin(_ sender: Any) {
        print("login with facebook")
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self){
            result in
            switch result{
            case .failed(let error):
                print(error.localizedDescription)
            case .cancelled:
            print("user cancelled login")
            case .success(_,_,_):
                self.getuserInfo { userInfo, error in
                    if let error = error {
                        print(error.localizedDescription)
                    }
                    if let userInfo = userInfo, let id = userInfo["id"],let name = userInfo["name"],let email = userInfo["email"] {
                        self.id.text = "ID:\(id)"
                        self.name.text = "Name:\(name)"
                        self.email.text = "email:\(email)"
                    }
                }
            }
        }
    }

    func getuserInfo(completion: @escaping (_ : [String:Any]? ,_ : Error?)-> Void) {
        let request = GraphRequest(graphPath: "me", parameters: ["fields" : "id, name, email,picture.type(large)"])
        request.start { (connection, result, error) in
    
            if let err = error { print(err.localizedDescription); return } else {
            if let fields = result as? [String:Any], let name = fields["name"] as? String, let id = fields["id"] as? String, let email = fields["email"] as? String , let profile = fields["picture"]   {
                self.id.text = "ID:\(id)"
                self.name.text = "Name:\(name)"
                self.email.text = "email:\(email)"
                let facebookProfileString = ((fields["picture"] as? [String:Any])?["data"] as? [String:Any])?["url"] as? String
                let facebookProfile = URL(string: facebookProfileString!)
                let imgData = try? Data(contentsOf: facebookProfile!)
                if let imgData = imgData{
                    let profile = UIImage(data: imgData as Data)
                    self.profile.image = profile
                print(name, id, facebookProfileString,email)

                }
            }

        }
     }
}


    @IBAction func ClickWithAppleID(_ sender: Any) {
        print("Login with Apple ID")
        
    }

    @IBAction func clickWithPhoneNumber(_ sender: Any) {
        print("Login with Phone Number")
    }
  }
