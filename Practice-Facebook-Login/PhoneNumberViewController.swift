//
//  PhoneNumberViewController.swift
//  Practice-Facebook-Login
//
//  Created by Hoeng Linghor on 11/26/20.
//

import UIKit
import FirebaseCore
import FirebaseAuth
class PhoneNumberViewController: UIViewController {

    @IBOutlet weak var OTP: UITextField!
    @IBOutlet weak var phone: UITextField!
    
    let userDefault = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

           //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
           //tap.cancelsTouchesInView = false

           view.addGestureRecognizer(tap)
       }

       //Calls this function when the tap is recognized.
       @objc func dismissKeyboard() {
           //Causes the view (or one of its embedded text fields) to resign the first responder status.
           view.endEditing(true)
    }
    

    @IBAction func verify(_ sender: Any) {
        guard let otpCode = OTP.text else {
            return
        }
        guard let verificationId = userDefault.string(forKey: "verficationId") else {
            return
        }
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationId, verificationCode: otpCode)
        Auth.auth().signInAndRetrieveData(with: credential) { (success, error) in
            if error == nil {
                print(success)
                print("-----sign in-----")
            }else{
                print("something went wrong  \(error?.localizedDescription)")
            }
        }
    }
    @IBAction func submitAction(_ sender: Any) {
        guard let phoneNumber = phone.text else {
            return }
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationId, error) in
            if error == nil {
                print(verificationId)
                guard let verifyId = verificationId else {
                    return
                }
                self.userDefault.set(verifyId, forKey: "verficationId")
                self.userDefault.synchronize()
            }else{
                print("unable to get secret verfication code from firebase",error?.localizedDescription)
            }
        }
  }
}
