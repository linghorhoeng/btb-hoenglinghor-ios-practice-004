//
//  AppleViewController.swift
//  Practice-Facebook-Login
//
//  Created by Hoeng Linghor on 11/27/20.
//

import UIKit
import FirebaseAuth
import AuthenticationServices
class AppleViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpBtn()
        // Do any additional setup after loading the view.
            }
    
    func setUpBtn(){
        let button = ASAuthorizationAppleIDButton()
        button.addTarget(self, action: #selector(handleSignIn), for: .touchUpInside)
        button.center = view.center
        view.addSubview(button)
    }
    @objc func handleSignIn(){
        goSignIn()
    }
    func goSignIn()  {
        let request = createAppleIDRequest()
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    func createAppleIDRequest()-> ASAuthorizationAppleIDRequest{
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let nounce = randomNonceString()
        request.nonce = sha256(nounce)
        currentNonce = nounce
        return request
    }
}
extension AppleViewController: ASAuthorizationControllerDelegate{
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential{
            guard let nounce = currentNonce else {
                fatalError("Invalid state: a login callback was received , but no login request was sent")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("unable to fetch identity token")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("unable to serialize token string from data\(appleIDToken.debugDescription)")
                return
            }
            let credential = OAuthProvider.credential(withProviderID: "apple.com", idToken: idTokenString, rawNonce: nounce, accessToken: nil)
            Auth.auth().signIn(with: credential) { (authDataResult, error) in
                if let user = authDataResult?.user{
                    print("====sign in \(user.uid), email:\(user.email ?? "unknown")")
                }
            }
        }
    }
}
extension AppleViewController: ASAuthorizationControllerPresentationContextProviding{
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
    // Adapted from https://auth0.com/docs/api-auth/tutorials/nonce#generate-a-cryptographically-random-nonce
    private func randomNonceString(length: Int = 32) -> String {
      precondition(length > 0)
      let charset: Array<Character> =
          Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
      var result = ""
      var remainingLength = length

      while remainingLength > 0 {
        let randoms: [UInt8] = (0 ..< 16).map { _ in
          var random: UInt8 = 0
          let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
          if errorCode != errSecSuccess {
            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
          }
          return random
        }

        randoms.forEach { random in
          if remainingLength == 0 {
            return
          }

          if random < charset.count {
            result.append(charset[Int(random)])
            remainingLength -= 1
          }
        }
      }

      return result
    }
    import CryptoKit

    // Unhashed nonce.
    fileprivate var currentNonce: String?

    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
      let inputData = Data(input.utf8)
      let hashedData = SHA256.hash(data: inputData)
      let hashString = hashedData.compactMap {
        return String(format: "%02x", $0)
      }.joined()

      return hashString
    }
